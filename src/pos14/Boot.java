/*
    Planetary Orbit Simulator 2014 (POS14)
    Copyright (C) 2014 Philippe Jaleev, Jarrett Wener, & Harrison Sarobidy Andriamanantena

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package pos14;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import momentbox.InitMoment;
import doppbox.InitDopp;
import gravitybox.InitGravity;

public class Boot extends JFrame implements ActionListener{
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				Boot b = new Boot();
			}
		});
	}

	public Boot(){
		setSize(400,100);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Planetary Orbit Simulator 2014");
		setLayout(new FlowLayout());
		
		JButton gravity = new JButton("GravityBox");
		JButton moment = new JButton("MomentumBox");
		JButton dopp = new JButton("DopplerexBox");
		JButton quit = new JButton("Exit");
		
		gravity.setActionCommand("gravity");
		gravity.addActionListener(this);
		moment.setActionCommand("moment");
		moment.addActionListener(this);
		dopp.setActionCommand("dopp");
		dopp.addActionListener(this);
		quit.setActionCommand("quit");
		quit.addActionListener(this);
		
		add(gravity);
		add(moment);
		add(dopp);
		add(quit);
	}
	
	public void actionPerformed(ActionEvent e) {
        if("gravity".equals(e.getActionCommand())) {
        	SwingUtilities.invokeLater(new Runnable(){
    			public void run(){
    				InitGravity b = new InitGravity();
    			}
    		});
        	dispose();
        }else if("moment".equals(e.getActionCommand())) {
        	SwingUtilities.invokeLater(new Runnable(){
    			public void run(){
    				InitMoment b = new InitMoment();
    			}
    		});
        	dispose();
        }else if("dopp".equals(e.getActionCommand())) {
        	SwingUtilities.invokeLater(new Runnable(){
    			public void run(){
    				InitDopp b = new InitDopp();
    			}
    		});
        	dispose();
        }else if("quit".equals(e.getActionCommand())) {
        	System.exit(0);
        }
	}
}