package momentbox;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.math.*;
import java.text.DecimalFormat;

import javax.swing.*;

import java.awt.*;

public class mainScreen extends JComponent implements ActionListener, MouseListener, momentumInterface, KeyListener{
	demoClass test = mainClass;
	boolean momentumCalculated = false;
	float Xpos1 = 0;
	float Ypos1 = 0;
	float Xpos2 = 0;
	float Ypos2 = 0;
	
	boolean exit = false;
	
	double xPosString;
	double yPosString;
	
	int globalStartTime;
	int overallTime = 1;
	int finishTime;
	
	int RGB;
	int RGB2;
	
	GeneralPath p;
	GeneralPath p2;
	
	asteroid displayAsteroid = test.asteroid1;
	
	public mainScreen()	{
		addMouseListener(this);
		addKeyListener(this);
	}
	
	public void getSpot(float xOne, float xTwo, float yOne, float yTwo)	{
		Xpos1 = xOne+200;
		Ypos1 = yOne;
		Xpos2 = xTwo+200;
		Ypos2 = yTwo;
		xPosString = Xpos1;
		yPosString = Ypos1;
		JButton nextBtn = new JButton("Switch");
		JButton pauseBtn = new JButton("Pause");
		JButton exitBtn = new JButton("Exit");
		JPanel buttonPanel = new JPanel();
		test.frame.add(buttonPanel, BorderLayout.PAGE_END);
		GridLayout gLayout  = new GridLayout(1,4,50,1);
		buttonPanel.setLayout(gLayout);
		buttonPanel.add(pauseBtn);
		buttonPanel.add(nextBtn);
		buttonPanel.add(new JPanel());
		buttonPanel.add(exitBtn);
		exitBtn.addActionListener(this);
		nextBtn.addActionListener(this);
		pauseBtn.addActionListener(this);
		RGB  = 255-(int)test.asteroid1.getMass()*3;
		RGB2  = 255-(int)test.asteroid2.getMass()*3;
		if (RGB <= 0)
			RGB = 0;
		if (RGB2 <= 0)
			RGB2 = 0;
		p = test.asteroid1.path;
		p.moveTo(Xpos1+17.5, Ypos1+17.5);
		p2 = test.asteroid2.path;
		p2.moveTo(Xpos2+17.5, Ypos2+17.5);
	}
	
	double [] n = new double[2];
	double [] t = new double[2];
	double V1n;
	double V1t;
	double V2n;
	double V2t;
	
	double V1nf;
	double V2nf;
	
	double [] V1nfVector = new double [2];
	double [] V2nfVector = new double [2];
	double [] V1tfVector = new double [2];
	double [] V2tfVector = new double [2];
	
	double [] V1f = new double[2];
	double [] V2f = new double[2];
	
	boolean tested = false;
	
	boolean pause = false;
	String angleDirection = " down";
	String oppositeAngleDirection = " up";
	
	boolean fixed = false;
	
	public void paint(Graphics g){
	    super.paintComponent(g);
	    int orginalStartTime = (int)(System.currentTimeMillis());
	    if (overallTime != 0) {
	    if (pause == false) {
	    	Xpos1 += overallTime*test.asteroid1.getxVelocity()/50;
	    	Ypos1 += overallTime*test.asteroid1.getyVelocity()/50;
	    	Xpos2 += overallTime*test.asteroid2.getxVelocity()/50;
	    	Ypos2 += overallTime*test.asteroid2.getyVelocity()/50;
	    	test.asteroid1.setXPos((int)Xpos1);
	    	test.asteroid1.setYPos((int)Ypos1);
	    	test.asteroid2.setXPos((int)Xpos2);
	    	test.asteroid2.setYPos((int)Ypos2);
	    	if (Xpos1 >= test.frame.getWidth()-radius || Xpos1 < 200){
	    		test.asteroid1.collidedSetXvelocity(-test.asteroid1.getxVelocity());
	    		momentumCalculated = false;
	    		if (Xpos1 >= test.frame.getWidth()-radius)
	    			Xpos1 = test.frame.getWidth()-radius-1;
	    		if (Xpos1 < 200)
	    			Xpos1 = 201;
	    	}
	    	if (Ypos1 >= test.frame.getHeight()-radius-75 || Ypos1 < 0){
	    		test.asteroid1.collidedSetYvelocity(-test.asteroid1.getyVelocity());
	    		momentumCalculated = false;
	    		if (Ypos1 >= test.frame.getHeight()-radius-75)
	    			Ypos1 = test.frame.getHeight()-radius-76;
	    		if (Ypos1 < 0)
	    			Ypos1 = 1;
	    	}
	    	if (Xpos2 >= test.frame.getWidth()-radius || Xpos2 < 200){ //good one!
	    		test.asteroid2.collidedSetXvelocity(-test.asteroid2.getxVelocity());
	    		momentumCalculated = false;
	    		if (Xpos2 >= test.frame.getWidth()-radius)
	    			Xpos2 = test.frame.getWidth()-radius-1;
	    		if (Xpos2 < 200)
	    			Xpos2 = 201;
	    	}
	    	if (Ypos2 >= test.frame.getHeight()-radius-75 || Ypos2 < 0)	{
	    		test.asteroid2.collidedSetYvelocity(-test.asteroid2.getyVelocity());
	    		momentumCalculated = false;
	    		if (Ypos2 >= test.frame.getHeight()-radius-75)
	    			Ypos2 = test.frame.getHeight()-radius-76;
	    		if (Ypos2 < 0)
	    			Ypos2 = 1;
	    	}
	    }
	    
	    Graphics2D g2 = (Graphics2D) g;
	    
	    if (Math.sqrt(Math.abs(Xpos1 - Xpos2)*Math.abs(Xpos1 - Xpos2) + Math.abs(Ypos1 - Ypos2)*Math.abs(Ypos1 - Ypos2)) <= radius && momentumCalculated == false) {
	    	n[0] = (Xpos2-Xpos1)/Math.sqrt((Xpos2-Xpos1)*(Xpos2-Xpos1) + (Ypos2-Ypos1)*(Ypos2-Ypos1));
	    	n[1] = (Ypos2-Ypos1)/Math.sqrt((Xpos2-Xpos1)*(Xpos2-Xpos1) + (Ypos2-Ypos1)*(Ypos2-Ypos1)); 
	    	t[0] = -n[1];
	    	t[1] = n[0];
	    	V1n = test.asteroid1.getxVelocity()*n[0] + test.asteroid1.getyVelocity()*n[1];
	    	V1t = test.asteroid1.getxVelocity()*t[0] + test.asteroid1.getyVelocity()*t[1];
	    	V2n = test.asteroid2.getxVelocity()*n[0] + test.asteroid2.getyVelocity()*n[1];
	    	V2t = test.asteroid2.getxVelocity()*t[0] + test.asteroid2.getyVelocity()*t[1];
	    	V1nf = (V1n*(test.asteroid1.getMass() - test.asteroid2.getMass())+2*test.asteroid2.getMass()*V2n)/(test.asteroid1.getMass() + test.asteroid2.getMass());
	    	V2nf = (V2n*(test.asteroid2.getMass() - test.asteroid1.getMass())+2*test.asteroid1.getMass()*V1n)/(test.asteroid2.getMass() + test.asteroid1.getMass());
	    	V1nfVector[0] = V1nf*n[0];
	    	V1nfVector[1] = V1nf*n[1];
	    	V2nfVector[0] = V2nf*n[0];
	    	V2nfVector[1] = V2nf*n[1];
	    	V1tfVector[0] = V1t*t[0];
	    	V1tfVector[1] = V1t*t[1];
	    	V2tfVector[0] = V2t*t[0];
	    	V2tfVector[1] = V2t*t[1];
	    	V1f[0] = V1nfVector[0] + V1tfVector[0];
	    	V1f[1] = V1nfVector[1] + V1tfVector[1];
	    	V2f[0] = V2nfVector[0] + V2tfVector[0];
	    	V2f[1] = V2nfVector[1] + V2tfVector[1];
	    	test.asteroid1.collidedSetXvelocity(V1f[0]);
	        test.asteroid1.collidedSetYvelocity(V1f[1]);
	        test.asteroid2.collidedSetXvelocity(V2f[0]);
	        test.asteroid2.collidedSetYvelocity(V2f[1]);
	        test.asteroid1.setcurrentV(Math.sqrt(V1f[0]*V1f[0] + V1f[1]*V1f[1]));
	        test.asteroid2.setcurrentV(Math.sqrt(V2f[0]*V2f[0] + V2f[1]*V2f[1]));

	        momentumCalculated = true;
	    }
	    
	    
	    Shape asteroidCircle1 = new Ellipse2D.Float(Xpos1, Ypos1, radius, radius);
 	    Shape asteroidCircle2 = new Ellipse2D.Float(Xpos2, Ypos2, radius, radius);
 	    
 	    if (Math.abs(test.asteroid1.getxVelocity()) >= Math.abs(test.asteroid1.getyVelocity()))
       	test.asteroid1.setInitialAngle(Math.atan(test.asteroid1.getyVelocity()/test.asteroid1.getxVelocity())*57.2957795);
        else
       	test.asteroid1.setInitialAngle(180-Math.atan(test.asteroid1.getyVelocity()/test.asteroid1.getxVelocity())*57.2957795);
 	    
        if (Math.abs(test.asteroid2.getxVelocity()) >= Math.abs(test.asteroid2.getyVelocity()))
       	test.asteroid2.setInitialAngle(Math.atan(test.asteroid2.getyVelocity()/test.asteroid2.getxVelocity())*57.2957795);
        else
       	test.asteroid2.setInitialAngle(180-Math.atan(test.asteroid2.getyVelocity()/test.asteroid2.getxVelocity())*57.2957795);
 	    DecimalFormat numberFormat = new DecimalFormat("#.00");
 	    
 	    g2.draw(p);
	    g2.draw(p2);
	    g2.setColor(new Color(RGB, RGB, RGB));
	    g2.fill(asteroidCircle1);
	    g2.setColor(new Color(RGB2, RGB2, RGB2));
	    g2.fill(asteroidCircle2);
	    
	    if (displayAsteroid == test.asteroid1){
	    	g2.setColor(Color.black);
	    	g2.draw(asteroidCircle2);
	    	g2.setColor(Color.RED);
	    	g2.draw(asteroidCircle1);
	    }
	    else if (displayAsteroid == test.asteroid2){
	    	g2.setColor(Color.black);
	    	g2.draw(asteroidCircle1);
	    	g2.setColor(Color.RED);
	    	g2.draw(asteroidCircle2);
	    }
	    g2.setColor(Color.black);
	    g2.drawLine(200, 0, 200, 575);
	    g2.drawLine(0, 575, 950, 575);
	    
	    if (fixed == false){
	    	if(displayAsteroid == test.asteroid1) {displayAsteroid = test.asteroid2;}else{displayAsteroid = test.asteroid1;}
	    	fixed = true;
	    }
	    
	    if (displayAsteroid == test.asteroid1) {
		    g2.drawString(asteroid1.getName(), 70, 30);
		    g2.drawString("Object's mass: " + numberFormat.format(asteroid1.getMass()), 30, 70);
		    g2.drawString("Object's Velocity: " + numberFormat.format(asteroid1.getVelocity()), 25, 110);
		    g2.drawString("X Position: " + numberFormat.format(xPosString-200), 25, 150);
		    g2.drawString("Y Position: " + numberFormat.format(yPosString), 25, 190);
		    g2.setStroke(new BasicStroke(1));
		    p.lineTo(asteroid1.getCurrentX()+radius/2, asteroid1.getCurrentY()+radius/2);
		    p2.lineTo(asteroid2.getCurrentX()+radius/2, asteroid2.getCurrentY()+radius/2);
		    xPosString = asteroid1.getCurrentX();
		    yPosString = asteroid1.getCurrentY();
		    
		    if (Math.abs(asteroid1.getxVelocity()) >= Math.abs(asteroid1.getyVelocity()))
		    	angleDirection = " down";
		    else
		    	angleDirection = " up";
		    
		    if (angleDirection == " up")
		    	oppositeAngleDirection = " down";
		    else
		    	oppositeAngleDirection = " up";
		    
		    if (asteroid1.getInitialAngle() < 0)
		    	g2.drawString("Angle From Horizontal: " + numberFormat.format(-asteroid1.getInitialAngle()) +(char)176 + oppositeAngleDirection, 7, 230);
		    else
		    	g2.drawString("Angle From Horizontal: " + numberFormat.format(asteroid1.getInitialAngle()) +(char)176 + angleDirection, 7, 230);
	    }
	    
	    if (displayAsteroid == test.asteroid2){
		    g2.drawString(asteroid2.getName(), 70, 30); //work only for as1 (might need to use inefficient solution)
		    g2.drawString("Object's mass: " + numberFormat.format(asteroid2.getMass()), 30, 70);
		    g2.drawString("Object's Velocity: " + numberFormat.format(asteroid2.getVelocity()), 25, 110);
		    g2.drawString("X Position: " + numberFormat.format(xPosString-200), 25, 150);
		    g2.drawString("Y Position: " + numberFormat.format(yPosString), 25, 190);
		    g2.setStroke(new BasicStroke(1));
		    p.lineTo(asteroid1.getCurrentX()+radius/2, asteroid1.getCurrentY()+radius/2);
		    p2.lineTo(asteroid2.getCurrentX()+radius/2, asteroid2.getCurrentY()+radius/2);
		    xPosString = asteroid2.getCurrentX();
		    yPosString = asteroid2.getCurrentY();
		    if (Math.abs(asteroid2.getxVelocity()) >= Math.abs(asteroid2.getyVelocity())) //change down/up to a String (not magic)
		    	angleDirection = " down";
		    else
		    	angleDirection = " up";
		    
		    if (angleDirection == " up")
		    	oppositeAngleDirection = " down";
		    else
		    	oppositeAngleDirection = " up";
		    
		    if (asteroid2.getInitialAngle() < 0)
		    	g2.drawString("Angle From Horizontal: " + numberFormat.format(-asteroid2.getInitialAngle()) +(char)176 + oppositeAngleDirection, 7, 230);
		    else
		    	g2.drawString("Angle From Horizontal: " + numberFormat.format(asteroid2.getInitialAngle()) +(char)176 + angleDirection, 7, 230);
		    }
	    overallTime = 1;
	    }
	    overallTime += (int)(System.currentTimeMillis()) - orginalStartTime;
	    if (exit == false)
	    	repaint(0,0,2000,2000);
	    else
	    	System.exit(0);
	}
	
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
			case "Exit": exit = true; break;
			case "Switch": if(displayAsteroid == test.asteroid1) {displayAsteroid = test.asteroid2;}else{displayAsteroid = test.asteroid1;} break;
			case "Pause": if (pause == false) pause = true;else pause = false; break;
		}
	}
	
	double x;
	double y;
	
	public void mouseClicked(MouseEvent arg0) {}
	public void mouseEntered(MouseEvent arg0) {}
	public void mouseExited(MouseEvent arg0) {}
	
	public void mousePressed(MouseEvent e) {}
	
	public void mouseReleased(MouseEvent arg0) {
		PointerInfo a = MouseInfo.getPointerInfo();
        Point b = a.getLocation();
        x = b.getX()-65;
        y = b.getY()-20;
        double xComponent= x-displayAsteroid.getCurrentX()-radius/2;
        double yComponent = y-displayAsteroid.getCurrentY()-radius/2;
        double theta = Math.acos(xComponent/Math.sqrt(xComponent*xComponent+(yComponent)*(yComponent)));
        
        if (Math.abs(x - test.asteroid1.getCurrentX()) <= radius && Math.abs(y - test.asteroid1.getCurrentY()) <= radius){
        	displayAsteroid = test.asteroid1;
        }
        else if (Math.abs(x - test.asteroid2.getCurrentX()) <= radius && Math.abs(y - test.asteroid2.getCurrentY()) <= radius){
        	displayAsteroid = test.asteroid2;
        }else if (y<displayAsteroid.getCurrentY()){
        	displayAsteroid.collidedSetXvelocity(displayAsteroid.getVelocity()*Math.cos(-theta));
        	displayAsteroid.collidedSetYvelocity(displayAsteroid.getVelocity()*Math.sin(-theta));
        }else{
        	displayAsteroid.collidedSetXvelocity(displayAsteroid.getVelocity()*Math.cos(theta));
        	displayAsteroid.collidedSetYvelocity(displayAsteroid.getVelocity()*Math.sin(theta));
        }
		momentumCalculated = false;
	}

	public void keyPressed(KeyEvent k) {}

	public void keyReleased(KeyEvent arg0) {}

	public void keyTyped(KeyEvent arg0) {}
}
