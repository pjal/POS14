package momentbox;

import java.awt.geom.GeneralPath;
import java.math.*;

public class asteroid extends demoClass implements momentumInterface
{
	private String name;
	private double mass;
	private double currentV;
	private float currentX;
	private float currentY;
	private double initialAngle;
	private double xVelocity;
	private double yVelocity;
	GeneralPath path;
	
	public asteroid(String newName, double newMass, double newcurrentV, float newcurrentX, float newcurrentY, double newInitialAngle){
		this.name = newName;
		this.mass = newMass;
		this.currentV = newcurrentV;
		this.currentX = newcurrentX;
		this.currentY = newcurrentY;
		this.initialAngle = newInitialAngle;
		path = new GeneralPath();
		path.moveTo(this.currentX+112, this.currentY+radius/2);
	}
	
	public void setName(String newName){
		this.name = newName;
	}
	
	public void setMass(double newMass){
		this.mass = newMass;
	}
	
	public void setcurrentV(double newcurrentV){
		this.currentV = newcurrentV;
	}
	
	public void setXPos(int newcurrentX){
		this.currentX = newcurrentX;
	}
	
	public void setYPos(int newcurrentY){
		this.currentY = newcurrentY;
	}
	
	public void setInitialAngle(double newInitialAngle){
		this.initialAngle = newInitialAngle;
	}
	
	public void setXvelocity(){
		this.xVelocity = Math.cos(this.initialAngle*(Math.PI/180))*this.currentV;
	}
	
	public void setYvelocity(){
		this.yVelocity = Math.sin(this.initialAngle*(Math.PI/180))*this.currentV;
	}
	
	public void collidedSetXvelocity(double newX){
		this.xVelocity = newX;
	}
	
	public void collidedSetYvelocity(double newY){
		this.yVelocity = newY;
	}
	
	public String getName(){
		return name;
	}
	
	public double getMass(){
		return mass;
	}
	
	public double getVelocity(){
		return currentV;
	}
	
	public float getCurrentX(){
		return this.currentX;
	}
	
	public float getCurrentY(){
		return this.currentY;
	}
	
	public double getInitialAngle(){
		return this.initialAngle;
	}
	
	public double getxVelocity(){
		return this.xVelocity;
	}
	
	public double getyVelocity(){
		return this.yVelocity;
	}
}
