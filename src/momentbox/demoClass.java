package momentbox;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.*;

public class demoClass extends JFrame implements ActionListener, momentumInterface {

	static JFrame frame = new JFrame("Momentum Sandbox");
	BorderLayout layout;
	JPanel dataPanel;
	JPanel mainPanel;
	
	JTextField nameField;
	JTextField massField;
	JTextField velocityField;
	JTextField xPositionField;
	JTextField yPositionField;
	JTextField directionField;
	JButton nextBtn;
	static int whichInput = 1;
	static asteroid whichAsteroid;
	static mainScreen display = new mainScreen();
	
	public demoClass(){
		this.setVisible(false);
		
		layout = new BorderLayout();
		dataPanel = new JPanel();
		mainPanel = new JPanel();
		
		nameField = new JTextField("First Object");
		massField = new JTextField("1");
		velocityField = new JTextField("1");
		xPositionField = new JTextField("0");
		yPositionField = new JTextField("100");
		directionField = new JTextField("0");

		setSize(420, 340);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridLayout inputLayout = new GridLayout(8, 3, 7, 20);
		setLayout(inputLayout);
		add(new JLabel());
		add(new JLabel("Momentum Data Input"));
		add(new JLabel());
		add(new JLabel("Name:"));
		add(nameField);
		add(new JLabel());
		add(new JLabel("Mass:"));
		add(massField);
		add(new JLabel("x10^5 Kg"));
		add(new JLabel("Initial Velocity:"));
		add(velocityField);
		add(new JLabel("km/s"));
		add(new JLabel("Initial X Position:"));
		add(xPositionField);
		add(new JLabel("km"));
		add(new JLabel("Initial Y Position:"));
		add(yPositionField);
		add(new JLabel("km"));
		add(new JLabel("Initial Direction:"));
		add(directionField);
		add(new JLabel("Degrees"));
		nextBtn = new JButton("Next"); //Button
		add(nextBtn);
		add(new JLabel());
		nextBtn.addActionListener(this);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLayout(layout);
        frame.setSize(950, 650);
        frame.getContentPane().add(display);
    }
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
			case "Next": case "Done":
				try{
					Double.parseDouble(massField.getText());
				} catch (NumberFormatException e1){
					massField.setText("Enter a Number");
				}
				try{
					Double.parseDouble(velocityField.getText());
				} catch (NumberFormatException e1){
					velocityField.setText("Enter a Number");
				}
				try{
					Double.parseDouble(xPositionField.getText());
				} catch (NumberFormatException e1){
					xPositionField.setText("Enter a Number");
				}
				try{
					Double.parseDouble(yPositionField.getText());
				} catch (NumberFormatException e1){
					yPositionField.setText("Enter a Number");
				}
				try{
					Double.parseDouble(directionField.getText());
				} catch (NumberFormatException e1){
					directionField.setText("Enter a Number");
				}
				
				if (whichInput == 1){
					whichAsteroid = asteroid1;
					nextBtn.setText("Done");
				}else
					whichAsteroid = asteroid2;
				
				whichAsteroid.setName(nameField.getText());
				whichAsteroid.setMass(Math.abs((Double)((Double.parseDouble(massField.getText())))));
				whichAsteroid.setcurrentV(Math.abs((Double)((Double.parseDouble(velocityField.getText())))));
				whichAsteroid.setXPos((int)((Double.parseDouble(xPositionField.getText()))));
				whichAsteroid.setYPos((int)((Double.parseDouble(yPositionField.getText()))));
				whichAsteroid.setInitialAngle((Double)((Double.parseDouble(directionField.getText()))));
				nameField.setText("Second Object"); massField.setText("1"); velocityField.setText("1"); xPositionField.setText("100"); yPositionField.setText("200"); directionField.setText("270"); 
				whichAsteroid.setXvelocity(); whichAsteroid.setYvelocity();
				whichInput++;
				if (whichInput > 2)	{
					display.getSpot(asteroid1.getCurrentX(), asteroid2.getCurrentX(), asteroid1.getCurrentY(), asteroid2.getCurrentY());
					frame.setVisible(true);
				}
		}
		
	}
}