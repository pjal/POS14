package gravitybox;

public class Engine{
	
	private final static double G_CONST = 6.67384e-11;
	
	public static Body[] update(Body[] bods, double speed) {
		for(Body bar : bods){
			double theta = 0;
			double[] Ag = {0,0};
			bar.Agtot=0;
			for(Body ent:bods){
				double distance = Math.sqrt(((bar.pos[0] - ent.pos[0])*(bar.pos[0] - ent.pos[0]))+((bar.pos[1] - ent.pos[1])*(bar.pos[1] - ent.pos[1])));						
				double Agt = 0;
				if(distance!=0){
					theta = Math.atan2(bar.pos[0]-ent.pos[0],bar.pos[1]-ent.pos[1]);
					Agt = (G_CONST * ent.mass) / (distance * distance);
					Ag[0] += Math.sin(theta) * Agt;
					Ag[1] += Math.cos(theta) * Agt;
				}
				bar.Agtot+=Agt;
			}
			bar.pos[0] += ((bar.vel[0] + (bar.vel[0] - Ag[0] * speed)) / 2 ) * speed;
			bar.pos[1] += ((bar.vel[1] + (bar.vel[1] - Ag[1] * speed)) / 2 ) * speed;
			bar.vel[0] -= Ag[0] * speed;
			bar.vel[1] -= Ag[1] * speed;
			int[] boo = box.toScale(bar.pos);
			bar.path.lineTo(boo[0], boo[1]);
		}
		return bods;
	}

}
