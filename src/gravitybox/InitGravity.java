package gravitybox;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class InitGravity extends JFrame implements ActionListener{
	int index = 0;
	
	JButton go = new JButton("Start");
	JButton previous = new JButton("Previous");
	JButton next = new JButton("Next");
	JButton del = new JButton("Delete");
	JPanel colourbox = new JPanel();
	
	JTextField red = new JTextField();
	JTextField green = new JTextField();
	JTextField blue = new JTextField();
	JTextField posx = new JTextField();
	JTextField posy = new JTextField();
	JTextField velx = new JTextField();
	JTextField vely = new JTextField();
	JTextField mass = new JTextField();
	JTextField rad = new JTextField();
	JTextField name = new JTextField();
	JTextField scale = new JTextField();
	
	double scale2 = 7e8;
	
	ArrayList<Body> bodies = new ArrayList<Body>(Arrays.asList(new Body(0.0,0.0,0.0,0.0,1.9891e30, 20, "Sol", Color.yellow),
			new Body(58980,0.0,0.0,46001200e3,3.3022e23, 5, "Mercury", Color.gray),
			new Body(35260,0.0,0.0,107477000e3,3.3022e23, 5, "Venus", new Color(139,119,101)),
			new Body(29782.0,0.0,0.0,149598261e3,5.9721986e24, 10, "Earth", Color.blue),
			new Body(24077,0.0,0.0,2279391e5,6.4185e23, 7, "Mars", Color.red)));
			
	public InitGravity(){
		setLayout(new GridLayout(11, 2));
		setBounds(30, 30, 400, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(false);
		setTitle("Data Entry");
		
		JPanel colourbox = new JPanel();
		colourbox.setLayout(new GridLayout(1,3));
		colourbox.add(red);
		colourbox.add(green);
		colourbox.add(blue);
		
		add(new JLabel("Position (x):"));
		add(posx);
		add(new JLabel("Position (y):"));
		add(posy);
		add(new JLabel("Velocity (x):"));
		add(velx);
		add(new JLabel("Velocity (y):"));
		add(vely);
		add(new JLabel("Mass:"));
		add(mass);
		add(new JLabel("Radius (in pixels):"));
		add(rad);
		add(new JLabel("Name:"));
		add(name);
		add(new JLabel("Scale:"));
		add(scale);
		add(new JLabel("Colour (RGB):"));
		add(colourbox);
		add(previous);
		add(next);
		add(go);
		add(del);
		
		previous.setEnabled(false);
		scale.setText(Double.toString(scale2));
		update();
		
		posx.setActionCommand("posx");
        posx.addActionListener(this);
        posy.setActionCommand("posy");
        posy.addActionListener(this);
        velx.setActionCommand("velx");
        velx.addActionListener(this);
        vely.setActionCommand("vely");
        vely.addActionListener(this);
        mass.setActionCommand("mass");
        mass.addActionListener(this);
        name.setActionCommand("name");
        name.addActionListener(this);
        rad.setActionCommand("rad");
        rad.addActionListener(this);
        scale.setActionCommand("scale");
        scale.addActionListener(this);
        next.setActionCommand("next");
        next.addActionListener(this);
        previous.setActionCommand("previous");
        previous.addActionListener(this);
        del.setActionCommand("del");
        del.addActionListener(this);
        go.setActionCommand("go");
        go.addActionListener(this);
        red.setActionCommand("re");
        red.addActionListener(this);
        green.setActionCommand("green");
        green.addActionListener(this);
        blue.setActionCommand("blue");
        blue.addActionListener(this);
	}
	
	private void update(){
		if(index==bodies.size()-1)
    		next.setText("New");
    	else if (index<0)
    		index = 0;
    	else
    		next.setText("Next");
		posx.setText(Double.toString(bodies.get(index).pos[0]));
		posy.setText(Double.toString(bodies.get(index).pos[1]));
		velx.setText(Double.toString(bodies.get(index).vel[0]));
		vely.setText(Double.toString(bodies.get(index).vel[1]));
		mass.setText(Double.toString(bodies.get(index).mass));
		rad.setText(Integer.toString(bodies.get(index).radius));
		name.setText(bodies.get(index).name);
		red.setText(Integer.toString(bodies.get(index).color.getRed()));
		green.setText(Integer.toString(bodies.get(index).color.getGreen()));
		blue.setText(Integer.toString(bodies.get(index).color.getBlue()));
	}
	
	public void actionPerformed(ActionEvent e) {
		try{
			if("posx".equals(e.getActionCommand())) {
				bodies.get(index).pos[0]=Double.parseDouble(posx.getText());
			}else if("posy".equals(e.getActionCommand())){
				bodies.get(index).pos[1]=Double.parseDouble(posy.getText());
			}else if("velx".equals(e.getActionCommand())){
				bodies.get(index).vel[0] = Double.parseDouble(velx.getText());
			}else if("vely".equals(e.getActionCommand())){
				bodies.get(index).vel[1] = Double.parseDouble(vely.getText());
			}else if("mass".equals(e.getActionCommand())){
				bodies.get(index).mass = Double.parseDouble(mass.getText());
			}else if("name".equals(e.getActionCommand())){
				bodies.get(index).name = name.getText();
			}else if("rad".equals(e.getActionCommand())){
				bodies.get(index).radius = Integer.parseInt(rad.getText());
			}else if("scale".equals(e.getActionCommand())){
				scale2 = Double.parseDouble(scale.getText());
			}else if("red".equals(e.getActionCommand()) || "green".equals(e.getActionCommand()) || "blue".equals(e.getActionCommand())){
				bodies.get(index).color = new Color(Integer.parseInt(red.getText()), Integer.parseInt(green.getText()), Integer.parseInt(blue.getText()));
			}else if("next".equals(e.getActionCommand())){
				index++;
	        	if(index==bodies.size()){
	     			bodies.add(new Body(0,0.0,0.0,0,0, 5, "testificate", Color.red));
	     			JOptionPane.showMessageDialog(null, "New entity \"testificate\" has been created.", "Entity Created", JOptionPane.INFORMATION_MESSAGE);
	     		}
        		previous.setEnabled(true);
			}else if("previous".equals(e.getActionCommand())){
				index--;
        		update();
        		if(index==0)
        			previous.setEnabled(false);
        		else
        			previous.setEnabled(true);
			}else if("del".equals(e.getActionCommand())){
				JOptionPane.showMessageDialog(null, "\"" + bodies.get(index).name + "\" has been deleted.", "Entity Deleted", JOptionPane.INFORMATION_MESSAGE);
	        	 bodies.remove(index);
	        	 index -= 1;
			}else if("go".equals(e.getActionCommand())){
	        	Body[] b = new Body[bodies.size()];
	        	b = bodies.toArray(b);
	        	Master m = new Master(b, scale2);
	        	dispose();
			}
		}catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(null,"Invalid information entered, recheck data.");
		}catch(IllegalArgumentException x){
			JOptionPane.showMessageDialog(null,"Invalid information entered, recheck data.");
		}
		update();
	} 
}
