package gravitybox;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class Master extends JFrame implements ActionListener{ 
	JDesktopPane desktop;
	
	static final int CONTROL_ROWS = 6;
	static final int CONTROL_COLUMS = 2;
	static final int ACC_MIN = 1;
	static final int ACC_MAX = 1000;
	static final int ACC_INIT = 100;
	
	static JPanel infoBox = new JPanel();
	static JTextField name = new JTextField();
	static JTextField mass = new JTextField();
	static JLabel vel = new JLabel();
	static JLabel grav = new JLabel();
	static final JSlider accel = new JSlider(JSlider.HORIZONTAL, ACC_MIN, ACC_MAX, ACC_INIT);
	static JButton exit = new JButton("Quit");
	static JButton down = new JButton("Previous");
	static JButton up = new JButton("Next");
	static int acceleration = 100;
	static boolean playing = false;
	static int index = 0;
	static Body[] bodies;
	static box b = new box();
	
	static DecimalFormat df = new DecimalFormat("###.##");
	
	static Thread t = new Thread() {
        public void run() {
        	startLoop(bodies);
        }
	};

	public Master(Body[] b2, double scale){
	    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	    setBounds(50, 50, screenSize.width  - 100, screenSize.height - 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("GravityBox");
		setResizable(false);
		setLayout(new BorderLayout());
		setVisible(true);
		
		infoBox.setLayout(new GridLayout(CONTROL_ROWS, CONTROL_COLUMS));
		infoBox.add(new JLabel("Object:"));
		infoBox.add(name);
		infoBox.add(new JLabel("Mass:"));
		infoBox.add(mass);
		infoBox.add(new JLabel("Velocity (km/s):"));
		infoBox.add(vel);
		infoBox.add(new JLabel("Acceleration (x10^-25 ms^-2):"));
		infoBox.add(grav);
		infoBox.add(down);
		infoBox.add(up);
		infoBox.add(accel);
		infoBox.add(exit);
		
		exit.setActionCommand("exit");
        exit.addActionListener(this);
		down.setActionCommand("down");
        down.addActionListener(this);
		up.setActionCommand("up");
        up.addActionListener(this);
		name.setActionCommand("name");
        name.addActionListener(this);
		mass.setActionCommand("mass");
        mass.addActionListener(this);
        
        bodies = b2;
        b.scale = scale;
		b.setBackground(Color.BLACK);
		down.setEnabled(false);
        add(infoBox, BorderLayout.PAGE_END);
		add(b, BorderLayout.CENTER);
        t.start();
        
		accel.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e) {
				acceleration = accel.getValue();						
			}
		});
	}

	private static void startLoop(Body[] bodies){
		for(Body b:bodies){
			b.generatePath();
		}
		double total = 0;
		double start = 0;
		boolean ba = true;
		while (true){
			b.bodies = bodies;
			start = System.currentTimeMillis();
			if(total>=1){
					update();
					if(b.time>=0.5 && ba){	//terrible hack
						for(Body b:bodies){
							b.generatePath();
						}
						ba=false;
					}
					bodies = Engine.update(bodies, total * acceleration);
					b.repaint();
					total = 0;
				}
				total += System.currentTimeMillis() - start;
				b.time=(b.time)+(total*acceleration*1.15741e-5);
		}
	}

	static public void update(){
		if(!name.hasFocus())
			name.setText(bodies[index].name);
		if(!mass.hasFocus())
			mass.setText(Double.toString(bodies[index].mass));
		vel.setText(df.format(Math.abs(bodies[index].vel[0])/1000+Math.abs(bodies[index].vel[1])/1000));
		grav.setText(df.format(bodies[index].Agtot*1e3));
	}

	public void actionPerformed(ActionEvent e) {
        if("exit".equals(e.getActionCommand())) {
            System.exit(0);
        }else if("mass".equals(e.getActionCommand())) {
        	try{
        		bodies[index].mass = Double.parseDouble(mass.getText());
        	}catch (NumberFormatException ex) {
    			JOptionPane.showMessageDialog(null,"Invalid information entered, recheck data.");
        	}
        }else if("name".equals(e.getActionCommand())){
        	bodies[index].name = name.getText();
        }else if("up".equals(e.getActionCommand())){
        	index++;
        	down.setEnabled(true);
    		if(index==bodies.length-1)
    			up.setEnabled(false);
        }else if("down".equals(e.getActionCommand())){
        	index--;
        	up.setEnabled(true);
    		if(index==0)
    			down.setEnabled(false);
        }
	}
}
