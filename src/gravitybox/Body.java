package gravitybox;

import java.awt.Color;
import java.awt.geom.GeneralPath;

public class Body {
	String name;
	double[] vel = new double[2];
	double[] pos = new double[2];
	double mass;
	double sma;
	double soi;
	double Agtot;
	Color color;
	GeneralPath path;
	int radius;
	
	public Body(double velX, double velY, double posX, double posY, double mass, int radius, String name, Color color){
		this.vel[0]=velX;
		this.vel[1]=velY;
		this.pos[0]=posX;
		this.pos[1]=sma=posY;
		this.mass = mass;
		this.name = name;
		this.color = color;
		this.radius = radius;
	}
	
	public void generatePath(){
		path = new GeneralPath();
		int[] boo = box.toScale(pos);
		path.moveTo(boo[0], boo[1]);
	}
}
