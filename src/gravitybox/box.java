package gravitybox;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.imageio.ImageIO;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class box extends JPanel{
	
	Body[] bodies;	
	static double scale = 0;
	static int height;
	static int width;
	static double time = 0;
	DecimalFormat df = new DecimalFormat("###.#");
	
	public void paintComponent(Graphics g) {
		height = super.getHeight();
		width = super.getWidth();
        super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
			for(Body bod : bodies){
				int[] pos = toScale(bod.pos);
				g2.setPaint(Color.gray);
				g2.setStroke(new BasicStroke(1));
				g2.draw(bod.path);
				int rad = bod.radius;
				Shape obj = new Ellipse2D.Float(pos[0]-rad, pos[1]-rad, rad*2, rad*2);
				g2.draw(obj);
				g2.setPaint(bod.color);
				g2.fill(obj);
			}
		g.drawString("Days simulated: " + df.format(time), width - 150, height-20);
		g2.dispose();
		g.dispose();
	}

	
	public static int[] toScale(double[] pos){
		return new int[] {(int)(pos[0]/scale+width/2),(int)(-pos[1]/scale+height/2)};
	}
}
