package doppbox;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;
import javax.swing.JComponent;

public class fistCanvas extends JComponent implements calculationsInterface, MouseListener {
     double relativeVelocityX;
     double relativeVelocityY; 
     double  observerXpos =500;
     double  observerYpos =600;
     double  emitterXpos =900;
     double emitterYpos=600;
     double finishTime=0;
     double overAllTime=0;
     double waveVelocity;
     ArrayList<Createwaves> ripples = new ArrayList<>();
     double originalStartTime; 
     int count =1;
     double waveFrequency=1;
     double observerFrequency=0;
     double waveLength;
     double waveNumber;
     double angularFrequency;
     double relativeVelocity;
     double dopplerWaveLength;
     double dopplerWaveNumber;
     double dopplerAngularFrequency;
     Color colour;
     double[] emitterVelocityArray = new double[3];
     double[] observerVelocityArray = new double[3];
     GeneralPath arrow = new GeneralPath();  
  public fistCanvas(){   
           addMouseListener(this);           
    }       
  public void setVariable(double waveVelocity,double  emitterVelocity,double emitterAngle,
  double originalStartTime, double observerVelocity, double observerAngle,Color colour, int waveLength){  
        ripples = new ArrayList<>();
        this.waveVelocity = waveVelocity;
        emitterVelocityArray[0]=  emitterVelocity*speedofLightRatio;
        emitterVelocityArray[1] =  emitterVelocityArray[0]*Math.cos((-emitterAngle*Math.PI)/180);
        emitterVelocityArray[2] =  emitterVelocityArray[0]*Math.sin((-emitterAngle*Math.PI)/180);
        this.colour = colour;
        this.waveLength = waveLength;
        waveFrequency = waveVelocity/waveLength;
        this.originalStartTime =originalStartTime;
        observerVelocityArray[0] = observerVelocity*speedofLightRatio;
        observerVelocityArray[1] = observerVelocityArray[0]*Math.cos((-observerAngle*Math.PI)/180);
        observerVelocityArray[2] = observerVelocityArray[0]*Math.sin((-observerAngle*Math.PI)/180);
        waveNumber= Math.PI*2/this.waveLength;
        angularFrequency = (1/waveFrequency)*waveNumber;
        dopplerWaveLength=waveLength;
        dopplerWaveNumber= waveNumber;
        dopplerAngularFrequency = angularFrequency;
        relativeVelocityX = observerVelocityArray[1] - emitterVelocityArray[1];
        relativeVelocityY =  observerVelocityArray[2] - emitterVelocityArray[2];
        relativeVelocity = Math.sqrt(relativeVelocityX*relativeVelocityX+relativeVelocityY*relativeVelocityY);    
        observerXpos =observerStartingXspot;
        observerYpos =observerStartingYspot;
        emitterXpos =emitterStartingXspot;
        emitterYpos=emitterstartingYspot;
      
    }
   
   
  double timepassed =1;
  public void paint(Graphics g) {
         g.setColor(getBackground());
         g.fillRect(0, 0, getWidth(), getHeight());
         setBackground(Color.BLACK);
         g.setColor(getForeground());
         Graphics2D g2 = (Graphics2D) g; 
         double startTime =(double) (System.currentTimeMillis()); 
         finishTime = (double) (System.currentTimeMillis()/1E3);
         double newTime = (double) (System.currentTimeMillis()/1E3);
         if(count%waveLength==0){
              double newemitpositionX =emitterXpos+10;
              double newemitpositionY= emitterYpos+10;
              ripples.add(new Createwaves());         
              ripples.get(ripples.size()-1).setTime(newTime);
              ripples.get(ripples.size()-1).setFrameFromCenter(newemitpositionX,
              newemitpositionY,newemitpositionX,newemitpositionY);              
          }        
         g2.setColor(colour);
         for(int index =0;index<ripples.size();index++){
              if(ripples.get(0).getHeight()>g.getClipBounds().width*2){
              ripples.remove(0);
          }   
            ripples.get(index).setFrameFromCenter(ripples.get(index).getCenterX(), ripples.get(index).getCenterY(),
            ripples.get(index).getCenterX()-waveVelocity*((newTime-ripples.get(index).getTime())), 
            ripples.get(index).getCenterY()+waveVelocity*(newTime-ripples.get(index).getTime()));           
            g2.draw(ripples.get(index));      
        }
         g2.setColor(Color.BLACK); 
         relativeVelocityX = Math.abs(observerVelocityArray[1]  - emitterVelocityArray[1]) ;
         relativeVelocityY =  Math.abs(observerVelocityArray[2]  - emitterVelocityArray[2]) ; 
         relativeVelocity = Math.sqrt(relativeVelocityX*relativeVelocityX+relativeVelocityY*relativeVelocityY); 
         double xComponent = Math.abs(observerXpos-emitterXpos); 
         double yComponent = Math.abs(observerYpos-emitterYpos);
         double xComponent2 = Math.abs(observerXpos+dt*observerVelocityArray[1] -(emitterXpos+dt*emitterVelocityArray[1])) ;
         double yComponent2 = Math.abs(observerYpos+dt* observerVelocityArray[2] -(emitterYpos+dt*emitterVelocityArray[2])) ;
         if((Math.sqrt((xComponent)*(xComponent)+(yComponent)*(yComponent))<Math.sqrt((xComponent2)*(xComponent2)
            +(yComponent2)*(yComponent2)))) { 
            observerFrequency = waveFrequency*Math.sqrt(Math.abs((1-relativeVelocity/SPEED_OF_LIGHT))/(1+relativeVelocity/SPEED_OF_LIGHT));//moving away
            }
          else{
           observerFrequency = waveFrequency*Math.sqrt(Math.abs((1+(relativeVelocity)/SPEED_OF_LIGHT))/(1-relativeVelocity/SPEED_OF_LIGHT));//moving towards 
         }
         dopplerWaveLength= waveVelocity/observerFrequency;
         dopplerWaveNumber= Math.PI*2/dopplerWaveLength;
         dopplerAngularFrequency = (1/observerFrequency)*dopplerWaveNumber;
         overAllTime = (finishTime-originalStartTime);
         timepassed =1;
         count++;  
         Ellipse2D emitter  = new Ellipse2D.Float((float) emitterXpos, (float) emitterYpos, 20, 20);
         Ellipse2D observer = new Ellipse2D.Float( (float)observerXpos,(float)  observerYpos, 20, 20);
         timepassed += ((double) (System.currentTimeMillis())) -startTime;
         if(emitterYpos<=5||emitterYpos>=canvas.getHeight()-15){
             emitterVelocityArray[2]=0;
         }
         if(emitterXpos<=5||emitterXpos>=canvas.getWidth()-15){
             emitterVelocityArray[1]=0;
         }
          
         if(observerXpos<=5||observerXpos>=canvas.getWidth()-15){
             observerVelocityArray[1]=0;
         }
          if(observerYpos<=5||observerYpos>=canvas.getHeight()-15){
             observerVelocityArray[2]=0;
         }  
        
         observerXpos += timepassed*observerVelocityArray[1] ;
         observerYpos += timepassed* observerVelocityArray[2] ;
         emitterXpos += timepassed*emitterVelocityArray[1] ;
         emitterYpos += timepassed*emitterVelocityArray[2] ;  
         
        g2.setColor(Color.YELLOW);
        g2.fill(emitter);
        g2.setColor(Color.BLUE);
        g2.fill(observer);
     }
      double getDopplerWaveLength(){
         return dopplerWaveLength;
      }
   
      double getDopplerWaveNumber(){
         return dopplerWaveNumber;
      }
    
        double getDopplerAngularFrequency(){
         return dopplerAngularFrequency;
      }
   
 
        double getWaveLength(){
         return waveLength;
      }
         double getAngularFrequency(){
          return angularFrequency;
      }
        double getWaveNumber(){
          return waveNumber;
      }
       double getWaveFrequency(){
         return waveFrequency;
     }
       double getOverAllTime(){
         return overAllTime;
     }
     
       double getObserverXpos(){
        
         return observerXpos;
     }
     
       double getObserverYpos(){
         return observerYpos;
     }
     
       double getEmitterXpos(){
         return emitterXpos;
     }
       double getEmitterYpos(){
         return emitterYpos;
     }
       
     public double getObserverXVelocity(){
         
         return observerVelocityArray[1];
     }
     
     public double getObserverYVelocity(){
         
         return  observerVelocityArray[2];
     }
     public double getEmiterXVelocity(){
       
         return emitterVelocityArray[1];
     }
     public double getEmiterYVelocity(){
       
         return emitterVelocityArray[2];
     }

     public double getWaveVelocity(){
         return waveVelocity;
     }


    public void mouseClicked(MouseEvent e) { 
         if(observerXpos<=15||observerXpos>=canvas.getWidth()-15||observerYpos<=15||observerYpos>=canvas.getHeight()-15){
             observerXpos =e.getXOnScreen();
             observerYpos =e.getYOnScreen()-10;
         }
    }

    
    public void mouseReleased(MouseEvent e) {        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    
    }
    public void mousePressed(MouseEvent e) {  
            double clickLocationX;
            double clickLocationY;
            PointerInfo a = MouseInfo.getPointerInfo();
            Point b = a.getLocation();
            clickLocationX = b.getX()-65;
            clickLocationY = b.getY()-20;
            double xComponent= clickLocationX-observerXpos+10;
            double yComponent = clickLocationY-observerYpos+10;
            double theta = Math.acos(xComponent/Math.sqrt(xComponent*xComponent+(yComponent)*(yComponent)));
            if(e.getX() >=observerXpos-20&& e.getX()<=observerXpos+20&&e.getY()>=observerYpos-20&&e.getY() <=observerYpos+20 || e.getClickCount() ==2){
               observerVelocityArray[0] = Math.abs(observerVelocityArray[0]);
               emitterVelocityArray[0] = Math.abs(emitterVelocityArray[0]);
               observerVelocityArray[1] =0;
               observerVelocityArray[2] =0;
            }  
            else if (clickLocationY<observerYpos){
                 observerVelocityArray[0] = Math.abs(observerVelocityArray[0]);
                 observerVelocityArray[1] =observerVelocityArray[0]*Math.cos(-theta);
                 observerVelocityArray[2] =observerVelocityArray[0]*Math.sin(-theta);     
            }
            
            else{
             observerVelocityArray[0] = Math.abs(observerVelocityArray[0]);
             observerVelocityArray[1] =observerVelocityArray[0]*Math.cos(theta);
             observerVelocityArray[2] =observerVelocityArray[0]*Math.sin(theta); 
            }
             relativeVelocityX = observerVelocityArray[1] - emitterVelocityArray[1];
             relativeVelocityY =  observerVelocityArray[2] - emitterVelocityArray[2];
             relativeVelocity = Math.sqrt(relativeVelocityX*relativeVelocityX+relativeVelocityY*relativeVelocityY); 
    }
 }
