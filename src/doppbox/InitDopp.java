package doppbox;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class InitDopp implements calculationsInterface{

	public InitDopp(){
		RevisedChangeVelocity frame = new RevisedChangeVelocity(); 
        frame.setVisible(true);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
 	   frame.setBounds(50, 50, screenSize.width  - 50*2, screenSize.height - 50*2);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        final int w = frame.getWidth();
        final int h = frame.getHeight();
        Thread t = new Thread() {
            public void run() {
         	   while(true){
                    canvas.repaint(0,0,w,h);
                }  
            }
    	};
    	t.start();
	}

}
