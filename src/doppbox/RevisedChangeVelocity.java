

package doppbox;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class RevisedChangeVelocity extends JFrame implements calculationsInterface{
    static JButton sendDataButton = new JButton("Play"); 
    static JButton stopButton = new JButton("Exit");
    double originalStartTime;
    double waveVelocity;
    double emitterVelocity = 30;
    double emitterAngle=0;
    double observerVelocity;
    double observerAngle;
    JTextField waveVelocityInput = new JTextField("300");
    JTextField emitterVelocityInput = new JTextField("0");
    JTextField emitterAngleInput = new JTextField("0");
    JTextField observerVelocityInput = new JTextField("0");
    JTextField observerAngleInput = new JTextField("0");
    ButtonGroup coloursGroup = new ButtonGroup();
    Color colour = Color.RED;
    int waveLength =80;
    InternalJFrameClass emitterGraphFrame = new InternalJFrameClass("Wave Frequency");
    InternalJFrameClass dopplerEffectGraphFrame = new InternalJFrameClass("Observed Wave Frequency");
    InternalJFrameClass inputFrame = new InternalJFrameClass("Input Variables");
    JButton showHideGraphButton = new JButton("Hide/Show Graphs");      
    static Boolean dopplerSimulationSelected = true;
    public  RevisedChangeVelocity(){
        setResizable(false);
        JLabel waveVelocityLabel= new JLabel("Wave Velocity:   1E6 m/s *");
        JLabel emitterVelocityLabel= new JLabel("Emitter Velocity:   1E6 m/s *");
        JLabel emitterAngleLabel = new JLabel("Emitter Angle");
        JLabel observerVelocityXLabel= new JLabel("Observer Velocity:  1E6 m/s *");
        JLabel observerAngleLabel = new JLabel("Observer Angle");
        inputFrame.setSize(350,350);
        inputFrame.setLocation(900, 20);
        inputFrame.setTitle("Input Variables");
        GridLayout layout = new GridLayout(10,2,12,1);
        inputFrame.setLayout(layout);
        inputFrame.add(waveVelocityLabel); 
        this.waveVelocityInput.setEditable(false);
        Dimension buttonsize = new Dimension(56,23);
        waveVelocityInput.setPreferredSize(buttonsize);
        inputFrame.add(waveVelocityInput);
        inputFrame.add(emitterVelocityLabel);
        emitterVelocityInput.setPreferredSize(buttonsize);
        inputFrame.add(emitterVelocityInput);
        inputFrame.add(emitterAngleLabel); 
        emitterAngleInput.setPreferredSize(buttonsize);
        inputFrame.add(emitterAngleInput);
        inputFrame.add(observerVelocityXLabel);
        inputFrame.add(observerVelocityInput);
        inputFrame.add(observerAngleLabel);
        inputFrame.add(observerAngleInput);
        inputFrame.setClosable(false);
        JRadioButton redButton = new JRadioButton("RED");
        JRadioButton orangeButton = new JRadioButton("ORANGE");
        JRadioButton yellowButton = new JRadioButton("YELLOW");
        JRadioButton greenButton = new JRadioButton("GREEN");
        JRadioButton blueButton = new JRadioButton("BLUE");
        JRadioButton indigoButton = new JRadioButton("INDIGO");
        JRadioButton violetButton = new JRadioButton("VIOLET"); 
        coloursGroup.add(redButton);
        coloursGroup.add(orangeButton);
        coloursGroup.add(yellowButton);
        coloursGroup.add(greenButton);  
        coloursGroup.add(blueButton);    
        coloursGroup.add(redButton);    
        coloursGroup.add(indigoButton);    
        coloursGroup.add(violetButton);            
        inputFrame.add(redButton);
        inputFrame.add(orangeButton);
        inputFrame.add(yellowButton);
        inputFrame.add(greenButton);  
        inputFrame.add(blueButton);    
        inputFrame.add(redButton);    
        inputFrame.add(indigoButton);    
        inputFrame.add(violetButton);
        JLabel blankLabel = new JLabel();
        inputFrame.add(blankLabel);
        inputFrame.add(sendDataButton);
        inputFrame.add(stopButton);
        add(inputFrame);
        inputFrame.setVisible(true);     
        add(emitterGraphFrame);
        emitterGraphFrame.add(graphCanvas);
        emitterGraphFrame.setBackground(Color.WHITE);
        emitterGraphFrame.setSize(600,200);
        emitterGraphFrame.setLocation(0, 30);
        emitterGraphFrame.setVisible(true);
        add(dopplerEffectGraphFrame);
        dopplerEffectGraphFrame.add(dopplerEffectGraphFramecanvas);
        dopplerEffectGraphFrame.setBackground(Color.WHITE);
        dopplerEffectGraphFrame.setSize(600,200);
        dopplerEffectGraphFrame.setLocation(0, 260);
        dopplerEffectGraphFrame.setVisible(true); 
        add(canvas, BorderLayout.CENTER); 
        canvas.setVisible(true);  
        JPanel lowerButtons = new JPanel();
        add(lowerButtons,BorderLayout.PAGE_END);
        lowerButtons.add(showHideGraphButton);
        showHideGraphButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                 lowerButtonsActionPerformed(evt);
              }
          });
        redButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                 sendDataButtonActionPerformed(evt);
              }
          });
        orangeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                 sendDataButtonActionPerformed(evt);
              }
          });
        yellowButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                 sendDataButtonActionPerformed(evt);
              }
          });
        greenButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                 sendDataButtonActionPerformed(evt);
              }
          });
        blueButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                 sendDataButtonActionPerformed(evt);
              }
          });
        indigoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                 sendDataButtonActionPerformed(evt);
              }
          });
        violetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                 sendDataButtonActionPerformed(evt);
              }
          });
        sendDataButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                 sendDataButtonActionPerformed(evt);
              }
          });
       stopButton.addActionListener(new java.awt.event.ActionListener() {  
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                 stopButtonActionPerformed(evt);
              }
          });
    } 


    public void sendDataButtonActionPerformed(ActionEvent e) { 
      
      emitterGraphFrame.setVisible(true);
      dopplerEffectGraphFrame.setVisible(true);
      switch (e.getActionCommand()){         
          case "RED" :  colour = Color.RED; waveLength = 80; break;
          case "ORANGE": colour = Color.ORANGE; waveLength = 70; break;
          case "YELLOW":colour = Color.YELLOW; waveLength = 60; break;
          case "GREEN":colour = Color.GREEN; waveLength = 50; break;
          case "BLUE":colour = Color.BLUE; waveLength = 40; break; 
          case "INDIGO":colour = Color.MAGENTA; waveLength = 30; break;
          case "VIOLET": colour = Color.PINK.darker(); waveLength = 20; 
       }
        try {
                waveVelocity = Double.parseDouble(waveVelocityInput.getText());
                emitterVelocity = Double.parseDouble(emitterVelocityInput.getText());
                emitterAngle = Double.parseDouble(emitterAngleInput.getText());
                observerVelocity = Double.parseDouble(observerVelocityInput.getText());
                observerAngle = Double.parseDouble(observerAngleInput.getText());
                originalStartTime = (double) (System.currentTimeMillis()/1E3);   
                if(observerVelocity>=waveVelocity||emitterVelocity>=waveVelocity){
                    JOptionPane.showMessageDialog(null,"Observer velocity can not be greater than the speed of light!");
                    throw new NumberFormatException();
                }
             }
        catch (NumberFormatException ex) {
                 JOptionPane.showMessageDialog(null,"Invalid Data");
                 throw new NumberFormatException();
        }  

        canvas.setVariable(waveVelocity, emitterVelocity, emitterAngle, originalStartTime,observerVelocity, observerAngle, colour, waveLength);       
        canvas.setVisible(true);
    }
    public void lowerButtonsActionPerformed(ActionEvent e){  
         if(emitterGraphFrame.isVisible()){
            emitterGraphFrame.setVisible(false);
            dopplerEffectGraphFrame.setVisible(false);
         }
         else {
            emitterGraphFrame.setLocation(0, 30);
            dopplerEffectGraphFrame.setLocation(0, 260);
            emitterGraphFrame.setVisible(true);
            dopplerEffectGraphFrame.setVisible(true);    
         }
     }         
     public void stopButtonActionPerformed(ActionEvent e)  {
    	 System.exit(0);
    }
  

}

