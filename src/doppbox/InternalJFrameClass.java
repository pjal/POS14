package doppbox;

import javax.swing.JInternalFrame;

/**
 *
 * @author andrians
 */
public class InternalJFrameClass extends JInternalFrame{
     
   public InternalJFrameClass(String title) {
        super(title, 
              false, //resizable
              true, //closable
              false, //maximizable
              false);//iconifiable
    }

}

