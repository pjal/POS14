package doppbox;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import javax.swing.JComponent;


public class DopplerEffectGraphCanvas extends JComponent implements calculationsInterface{
    int count=0;
    public DopplerEffectGraphCanvas() {
           
    }
    public void paint(Graphics g){
       Graphics2D g2 = (Graphics2D) g; 
       int graphSize=600;
       double[] x1Array = new double[graphSize];
       double[] y1Array = new double[graphSize];
       GeneralPath function = new GeneralPath(GeneralPath.WIND_EVEN_ODD,x1Array.length);
       GeneralPath xAxis = new GeneralPath();
       GeneralPath yAxis = new GeneralPath();
       xAxis.moveTo(0,80);
       xAxis.lineTo(600,80); 
       yAxis.moveTo(300,0);
       yAxis.lineTo(300,300);
       function.moveTo(x1Array[0],y1Array[0]); 
       for(int index =0;index<count&&index<graphSize;index++){ 
          if(RevisedChangeVelocity.sendDataButton.getModel().isPressed()){
               index=0;
               count=0; 
           } 
          double replaceVar = index;
          x1Array[index]=index;      
          y1Array[index]= 50*Math.sin(canvas.getDopplerWaveNumber()*replaceVar-canvas.getDopplerAngularFrequency()*canvas.getOverAllTime()*canvas.getWaveVelocity())+80;
          function.lineTo(x1Array[index],y1Array[index]); 
        }  
       g2.draw(xAxis);  
       g2.draw(yAxis);
       g2.draw(function);  
       count++;
    }
    
    
   
}

